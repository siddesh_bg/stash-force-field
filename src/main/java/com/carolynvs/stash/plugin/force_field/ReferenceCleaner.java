package com.carolynvs.stash.plugin.force_field;

/** Cleanup reference patterns which will always fail due to missing wildcards
 * e.g. foobar -> refs/foobar or refs/releases/ -> refs/releases/*
 */
public final class ReferenceCleaner
{
    private ReferenceCleaner(){}

    public static String fixUnmatchableRegex(String ref)
    {
        if (requiresWildcardPrefix(ref))
        {
            ref = applyWildcardPrefix(ref);
        }

        if (requiresWildcardSuffix(ref))
        {
            ref = applyWildcardSuffix(ref);
        }

        return ref;
    }

    private static boolean isFullyQualifiedReference(String ref)
    {
        return ref.startsWith("refs/");
    }

    private static boolean requiresWildcardPrefix(String ref)
    {
        return !isFullyQualifiedReference(ref) && !hasWildcardPrefix(ref);
    }

    private static boolean hasWildcardPrefix(String ref)
    {
        return ref.startsWith("**");
    }

    private static String applyWildcardPrefix(String ref)
    {
        return "**/" + ref;
    }

    private static boolean requiresWildcardSuffix(String ref)
    {
        // regex paths that end with a slash will always fail unless there is a wildcard suffix
        return ref.endsWith("/") || ref.endsWith("\\");
    }

    private static String applyWildcardSuffix(String ref)
    {
        return ref + "**";
    }
}
